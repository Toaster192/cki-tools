import argparse
import gitlab
import logging
import sys

from gitlab import GitlabAuthenticationError, GitlabGetError, GitlabListError

import utils
from retry import retry

# TODO make triggers installable and reuse the code from there

if __name__ == '__main__':
    logging.basicConfig(format='%(asctime)s %(levelname)s %(message)s',
                        level=logging.DEBUG,
                        stream=sys.stdout)
    logging.getLogger("requests").setLevel(logging.WARNING)
    logging.getLogger("urllib3").setLevel(logging.WARNING)
    parser = argparse.ArgumentParser(
        description='Get last successful pipeline'
    )
    parser.add_argument(
        '-b', '--cki-pipeline-branch',
        type=str,
        help='CKI pipeline branch',
        required=True,
    )
    parser.add_argument(
        '-t', '--cki-pipeline-type',
        type=str,
        help='CKI pipeline type',
    )
    parser.add_argument(
        '--project',
        type=str,
        help='Project where is hosted the CKI pipeline',
        default='cki-project/cki-pipeline',
    )
    args = parser.parse_args()
    gitlab_url = utils.get_env_var_or_raise('GITLAB_URL')
    private_token = utils.get_env_var_or_raise('GITLAB_PRIVATE_TOKEN')
    gitlab_instance = gitlab.Gitlab(gitlab_url,
                                    private_token=private_token,
                                    api_version=str(4))

    # don't catch GitlabAuthenticationError, that's pretty straight-forward
    project = retry(GitlabGetError)(gitlab_instance.projects.get)(args.project)

    page = 1
    while True:
        pipelines = retry(GitlabListError)(project.pipelines.list)(page=page,
                                                                   per_page=10)

        for pipeline in pipelines:
            if pipeline.attributes['status'] != 'success':
                continue
            vars = utils.get_variables(project, pipeline)
            if not vars:
                continue
            if vars['cki_pipeline_branch'] != args.cki_pipeline_branch:
                continue
            if args.cki_pipeline_type:
                if vars['cki_pipeline_type'] == args.cki_pipeline_type:
                    print(pipeline.attributes['id'])
                    sys.exit()
                else:
                    continue
            else:
                print(pipeline.attributes['id'])
                sys.exit()
        page += 1
