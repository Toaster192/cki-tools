"""Retrigger pipelines already run."""
import argparse
import sys
import logging
import time

import gitlab
from gitlab import GitlabGetError

import utils
# TODO make triggers installable and reuse the code from there

from retry import retry


def run_directly_tests(project, pipeline, variables):
    base_url = '{}/-/jobs/{{}}/artifacts/download'.format(
        project.web_url,
    )
    variables['skip_rhcheckpatch'] = 'true'
    variables['skip_build'] = 'true'
    variables['skip_merge'] = 'true'
    variables['skip_createrepo'] = 'true'
    for job in pipeline.jobs.list():
        if job.stage in ['publish', 'createrepo']:
            arch = job.name.split()[1]
            variables['ARTIFACT_URL_{}'.format(arch)] = base_url.format(job.id)


def retrigger(project, gitlab_pipeline_id, trigger_token, new_vars,
              tests_only):
    pipeline = project.pipelines.get(gitlab_pipeline_id)
    variables = utils.get_variables(project, pipeline)
    variables.update(new_vars or {})
    if variables.get('cki_pipeline_type', None):
        if 'cki_pipeline_type' not in new_vars:
            logging.warning('Prepending `retrigger-` to cki_pipeline_type ')
            new_type = 'retrigger-{}'.format(variables['cki_pipeline_type'])
            variables['cki_pipeline_type'] = new_type
    if 'mail_to' not in new_vars:
        logging.warning('Not sending email as mail_to variable is not set')
        variables['mail_to'] = ''
    if 'mail_add_maintainers_to' not in new_vars:
        logging.warning('Disabling mailing maintainers')
        variables['mail_add_maintainers_to'] = ''
    subject = variables.get('subject') or variables.get('title')
    subject = subject or "Empty subject"
    variables['subject'] = f'Retrigger: {subject}'
    commit_msg = pipeline.jobs.list()[0].attributes['commit']['message']
    commit_msg = commit_msg.splitlines()[0]
    commit_msg = f'Retrigger: {variables["cki_pipeline_type"]}: {commit_msg}'
    utils.create_commit(project, variables, commit_msg)
    if tests_only:
        run_directly_tests(project, pipeline, variables)
    new_pipeline = project.trigger_pipeline(variables['cki_pipeline_branch'],
                                            trigger_token, variables)
    logging.info('Pipeline "%s" triggered', commit_msg)
    return new_pipeline


def wait_for_pipeline(pipeline):
    time.sleep(30)
    retry(GitlabGetError)(pipeline.refresh)()
    while pipeline.attributes['status'] == 'running':
        time.sleep(60)
        print('.', end='')
        retry(GitlabGetError)(pipeline.refresh)()
    print()
    return pipeline.attributes['status'] == 'success'


class StoreNameValuePair(argparse.Action):
    def __call__(self, parser, namespace, values, option_string=None):
        variable = namespace.variable or {}
        for key_value_pair in values:
            key, value = key_value_pair.split('=')
            variable[key] = value
        namespace.variable = variable


if __name__ == '__main__':
    logging.basicConfig(format='%(asctime)s %(levelname)s %(message)s',
                        level=logging.DEBUG,
                        stream=sys.stdout)
    logging.getLogger("requests").setLevel(logging.WARNING)
    logging.getLogger("urllib3").setLevel(logging.WARNING)
    parser = argparse.ArgumentParser(description='Retrigger pipelines')
    parser.add_argument(
        '-p', '--pipeline-id',
        type=int,
        help='Gitlab pipeline id',
        required=True,
    )
    parser.add_argument(
        '--tests-only',
        help='Run only testing stage, skipping all the previous stages',
        action='store_true',
    )
    parser.add_argument(
        "--variable",
        action=StoreNameValuePair,
        nargs='+',
        help=(
            'Variable to replace in the new pipeline triggered '
            'e.g. --variable mail_to=nobody'
        ),
    )
    parser.add_argument('-w', '--wait',
                        help='Wait until the pipeline is completed',
                        action='store_true')
    parser.add_argument(
        '--project',
        type=str,
        help='Project where is hosted the CKI pipeline',
        default='cki-project/cki-pipeline',
    )
    args = parser.parse_args()
    gitlab_url = utils.get_env_var_or_raise('GITLAB_URL')
    private_token = utils.get_env_var_or_raise('GITLAB_PRIVATE_TOKEN')
    trigger_token = utils.get_env_var_or_raise('GITLAB_TRIGGER_TOKEN')
    gitlab_instance = gitlab.Gitlab(gitlab_url,
                                    private_token=private_token,
                                    api_version=4)
    project = gitlab_instance.projects.get(args.project)
    new_pipeline = retrigger(project, args.pipeline_id, trigger_token,
                             args.variable, args.tests_only)
    pipeline_url = '{}/pipelines/{}'.format(project.attributes['web_url'],
                                            new_pipeline.attributes['id'])
    logging.info('Pipeline url %s', pipeline_url)
    if args.wait:
        if not wait_for_pipeline(new_pipeline):
            sys.exit(new_pipeline.attributes['status'])
