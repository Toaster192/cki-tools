import time

def retry(exception, retries=3, initial_delay=3, logger=None):

    def wrapper(function):
        def wrapped(*args, **kwargs):
            wrapped.failed_count = 0
            wrapped.retries = retries

            delay = 0
            for i in range(0, retries):
                delay += initial_delay

                try:
                    return function(*args, **kwargs)
                except exception as e:
                    wrapped.failed_count += 1

                    s = str(e) + ", delaying for %ds" % delay
                    if logger:
                        logger.warning(s)
                    else:
                        print(s)

                    # don't sleep on last attempt, there's no point
                    if i != retries - 1:
                        time.sleep(delay)
                    else:
                        # reraise exc. instead
                        raise e

        return wrapped
    return wrapper
