import argparse
import os
import re
import sys
import logging
import subprocess

import gitlab

import utils
# TODO make triggers installable and reuse the code from there


def cancel_pipeline(project, pipeline):
    """
    Cancel a pipeline, canceling the beaker job as well.
    """
    logging.info('Cancelling pipeline %s', pipeline.id)
    pipeline.cancel()
    for job in pipeline.jobs.list():
        if job.stage == 'test':
            job = project.jobs.get(job.id)
            for beaker_job_id in re.findall('J:[0-9]+', job.trace().decode()):
                args = ['bkr', 'job-cancel', beaker_job_id]
                subprocess.check_call(args)


if __name__ == '__main__':
    logging.basicConfig(format='%(asctime)s %(levelname)s %(message)s',
                        level=logging.DEBUG,
                        stream=sys.stdout)
    logging.getLogger("requests").setLevel(logging.WARNING)
    logging.getLogger("urllib3").setLevel(logging.WARNING)
    parser = argparse.ArgumentParser(description='Retrigger pipelines')
    parser.add_argument(
        '-p', '--pipeline-id',
        type=int,
        help='Gitlab pipeline id',
        required=True,
    )
    parser.add_argument(
        '--project',
        type=str,
        help='Project where is hosted the CKI pipeline',
        default='cki-project/cki-pipeline',
    )
    args = parser.parse_args()
    gitlab_url = utils.get_env_var_or_raise('GITLAB_URL')
    private_token = utils.get_env_var_or_raise('GITLAB_PRIVATE_TOKEN')
    gitlab_instance = gitlab.Gitlab(gitlab_url,
                                    private_token=private_token,
                                    api_version=4)
    project = gitlab_instance.projects.get(args.project)
    pipeline = project.pipelines.get(args.pipeline_id)
    cancel_pipeline(project, pipeline)
