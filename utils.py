"""Generic functions and constants"""
import json
import os

import requests

from gitlab import GitlabListError

from retry import retry

class EnvVarNotSetError(Exception):
    """Requested environment variable is not set."""
    pass


def get_env_var_or_raise(name):
    """
    Retrieve the value of an environment variable. Raise EnvVarNotSetError if
    it's not set.

    Args:
        name:      Name of the variable's value to retrieve.

    Returns:
        Value of the environment variable, if set.

    Raises:
        EnvVarNotSetError if the variable is not set.
    """
    env_var = os.getenv(name)
    if not env_var:
        raise EnvVarNotSetError(f'Environment variable {name} is not set!')
    return env_var


def get_variables_api(project, pipeline):
    """
    Get the variables used by the corresponding pipeline.

    Args:
        project:  Object representing the GitLab project.
        pipeline: Object representing the pipeline to retrieve variables for.

    Returns:
        A dictionary representing pipeline's variables.
    """
    headers = {'Private-Token': os.environ['GITLAB_PRIVATE_TOKEN']}
    url = project._links['self'] + f'/pipelines/{pipeline.id}/variables'
    resp = requests.get(url, headers=headers)
    resp.raise_for_status()
    variables = {var['key']: var['value'] for var in resp.json()}
    return variables


@retry(Exception)
def get_variables(project, pipeline):
    """
    Get the variables used by the corresponding job. Use a no so disgusting
    hack because python-gitlab doesn't expose this attribute yet.

    Args:
        project:  Object representing the GitLab project.
        pipeline: Object representing the pipeline to retrieve variables for

    Returns:
        A dictionary representing job's variables.
    """
    version = [int(v) for v in project.manager.gitlab.version()[0].split('.')]
    if version >= [11, 11]:
        return get_variables_api(project, pipeline)
    try:
        job_id = pipeline.jobs.list(page=1, per_page=1)[0].attributes['id']
    except IndexError:
        # pipeline without any job associated (e.g. corrupted yaml)
        return {}

    job_url = '{}/-/jobs/{}.json'.format(project.attributes.get('web_url'),
                                         job_id)
    response = requests.get(job_url)
    if response.status_code != 200:
        raise Exception('Unexpected response {} from {} !'.format(
            response.status_code, job_url
        ))

    # when the pipeline was triggered by a commit
    try:
        variables = json.loads(response.content)['trigger']['variables']
    except KeyError:
        variables = []
    result = {}
    for var in variables:
        result[var['key']] = var['value']
    return result


@retry(GitlabListError)
def create_commit(project, trigger, title):
    """
    Create a commit in the branch where the pipeline will run. It's just used
    for getting an intuitive view using the gitlab interface.

    Args:
        project: GitLab project to create commit in.
        trigger: Dictionary with all data to include in the message.
        title:   String to use as the commit message title.
    """
    commit_message = title + '\n'
    for key, value in trigger.items():
        commit_message += '\n{} = {}'.format(key, value)
    data = {
        'branch': trigger['cki_pipeline_branch'],
        'commit_message': commit_message,
        'actions': [],
    }
    project.commits.create(data)
